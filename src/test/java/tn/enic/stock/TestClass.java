package tn.enic.stock;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.enic.stock.entities.Produit;
import tn.enic.stock.repositories.ProduitRepository;
import tn.enic.stock.service.ProduitService;
 
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestClass {
	
	@Autowired
	private ProduitService produitService;
	
	@Autowired
	private ProduitRepository produitrepo;

	@Test
	public void  testGetByStockAddressSucces() {
		
		produitrepo.save(new Produit("kkkkk", null, "jjj", null));
		assertEquals("a", produitService.findByStockAddress("a").get(0).getStock().getAddress());
	}
	
	@Test(expected = ArithmeticException.class)
	public void testTestExceptionSuccess() {
		produitService.testException();
	}
	
}
