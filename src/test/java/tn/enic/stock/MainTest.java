package tn.enic.stock;

import java.util.Arrays;
import java.util.List;

import tn.enic.stock.entities.Utilisateur;

public class MainTest implements MainTestInterface {

	public static void main(String[] args) {
		
		List<Utilisateur> list = 
				Arrays.asList(new Utilisateur("a","b","c","d"), 
						new Utilisateur("a2", "b2", "c2", "d"));

		list.forEach(Utilisateur::hkeya);
	}

	
	public static void hkeya(Utilisateur u) {
		
		System.out.println(u.getLogin());
	}
	
	public static void hkeya(Utilisateur u, Utilisateur u1) {
		
		System.out.println(u.getLogin());
	}
	
	public void hkeya2(Utilisateur u) {
		
		System.out.println(u.getLogin());
	}
}
