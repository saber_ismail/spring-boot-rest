package tn.enic.stock.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tn.enic.stock.entities.Produit;
import tn.enic.stock.entities.Stock;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

	List<Produit> findByType(String type);
 
	
	List<Produit> findByTypeAndStock(String type, Stock stock);
	
	/*@Query(value="insert into  Produit(id, libelle, type) values(:id , :libelle, :type)", nativeQuery = true)
	public void insertproduit(@Param("id") Long id,@Param("libelle")  String libelle,  @Param("type")  String type);*/
	
	@Modifying
	@Query(value="update  Produit p set libelle= :libelle, type= :type where id= :id ", nativeQuery = true)
	@Transactional
	void updateLibelleById(@Param("id") Long id,@Param("libelle")  String libelle,  @Param("type")  String type);
	
	Integer countByTypeAndStock(String type, Stock stock);

	List<Produit> findByStock_Address(String address);

}
