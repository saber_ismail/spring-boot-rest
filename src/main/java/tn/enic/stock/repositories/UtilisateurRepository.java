package tn.enic.stock.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import tn.enic.stock.entities.Utilisateur;

public interface UtilisateurRepository extends PagingAndSortingRepository<Utilisateur, Long> {

	public Utilisateur findByLoginAndPassword(String login, String password);
	
	public Utilisateur  save(Utilisateur utilisateur);
	
}
