package tn.enic.stock.repositories;

 
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.enic.stock.entities.Stock;

public interface StockRepository extends JpaRepository<Stock, Long> {
	
	  Optional<Stock> findById(Long id);
	  
	  List<Stock>  findByAddress(String address); 	
}
 