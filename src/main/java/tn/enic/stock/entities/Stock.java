package tn.enic.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Stock implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3153934111965922618L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String address;

	@OneToMany(mappedBy = "stock")
	private List<Produit> produitList;

	public Stock() {
		// TODO Auto-generated constructor stub
	}

	public Stock(String address, List<Produit> produitList) {
		this.address = address;
		this.produitList = produitList;
	}

	public Long getId() {
		return id;
	}

	public String getAddress() {
		return address;
	}

	public List<Produit> getProduitList() {
		return produitList;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setProduitList(List<Produit> produitList) {
		this.produitList = produitList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((produitList == null) ? 0 : produitList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stock other = (Stock) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (produitList == null) {
			if (other.produitList != null)
				return false;
		} else if (!produitList.equals(other.produitList))
			return false;
		return true;
	}

	
}
