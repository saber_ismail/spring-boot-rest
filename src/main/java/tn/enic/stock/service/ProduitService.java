package tn.enic.stock.service;

import java.util.List;

import tn.enic.stock.entities.Produit;

public interface ProduitService {

    void updateLibelleById(Long id, String libelle, String type);
   
	List<Produit> findByType(String type);
	
	List<Produit> findByStockAddress(String address);
	
	void testException();
	
}
