package tn.enic.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.enic.stock.entities.Produit;
import tn.enic.stock.repositories.ProduitRepository;
import tn.enic.stock.repositories.StockRepository;

@Service
public class ProduitServiceImpl implements ProduitService  {
	
	@Autowired
    private ProduitRepository repositoryProduit;
	
	@Autowired
    private StockRepository repositoryStock;
		 
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateLibelleById( Long id, String libelle,  String type) {
		 		 
       repositoryProduit.updateLibelleById(id, libelle, type);	
    }
	
	@Override
	public List<Produit> findByType(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Produit> findByStockAddress(String address) {
		
		return repositoryProduit.findByStock_Address(address);
	}
	
	
	public void testException() {
		
		throw new ArithmeticException();
	}
}
